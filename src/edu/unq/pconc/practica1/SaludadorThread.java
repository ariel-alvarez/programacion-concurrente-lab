package edu.unq.pconc.practica1;

import edu.unq.pconc.utils.ExtendedThread;

import static edu.unq.pconc.utils.ConcurrentUtils.dormir;
import static edu.unq.pconc.utils.ConcurrentUtils.print;

public class SaludadorThread extends ExtendedThread {

    private final String mensaje;
    private final int intervaloEnSegundos;

    public SaludadorThread(String mensaje, int intervaloEnSegundos) {
        this.mensaje = mensaje;
        this.intervaloEnSegundos = intervaloEnSegundos;
    }

    @Override
    public void run() {
        while (true){
            print(mensaje);
            dormir(intervaloEnSegundos);
        }
    }
}
