package edu.unq.pconc.practica1.molinetes;

import edu.unq.pconc.utils.ExtendedThread;
import static edu.unq.pconc.utils.ConcurrentUtils.print;

public class MolineteIncrementador extends ExtendedThread {
    @Override
    public void run() {
        for(int i=0;i<100000;i++){
            ContadorDeMolinetes.incMolinetes();
        }
        print(ContadorDeMolinetes.molinetes());
    }
}
