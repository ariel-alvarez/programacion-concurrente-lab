package edu.unq.pconc.practica1.molinetes;

/**
 * incMolinetes puede producir un race condition, porque el incremento no es atómico, sino que se separa en
 * aux <- cantMolinetes + 1
 * cantMolinetes <- aux
 *
 * Esto produce un race condition en donde, dados dos o más threads que intenten modificar cantMolinetes,
 * pueden pisarse los incrementos entre sí.
 *
 * Este incremento lo llamamos región crítica, porque es la instrucción que está modificando la memoria compartida.
 * -----------------------
 * No se ve en la clase, pero la forma básica en java de resolverlo es usar synchronize, que aplica un mutex sobre
 * el método. Hay que tener cuidado con esto porque puedo llegar a bloquear más cosas de las que necesito, disminuyendo
 * el nivel de concurrencia.
 *
 * ------------------------
 *
 * Volviendo a lo que se ve en la materia, lo que vamos a usar son mecanismos desarrollados manualmente, en lugar de
 * aprovechar los que trae java. En este caso, bastaría con implementar un mutex manualmente sobre un flag booleano.
 *
 *
 */
public class ContadorDeMolinetes {
    private static int cantMolinetes = 0;
    public /*synchronized*/ static void incMolinetes(){
        cantMolinetes += 1;
    }
    public static int molinetes(){
        return cantMolinetes;
    }
}
