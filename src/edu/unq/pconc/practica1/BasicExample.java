package edu.unq.pconc.practica1;

import edu.unq.pconc.utils.ConcurrentUtils;

public class BasicExample extends ConcurrentUtils {
    public static void main(String[] args){
        new SaludadorThread("Hola!", 2).start();
        new SaludadorThread("Pepe", 3).start();
    }

}
