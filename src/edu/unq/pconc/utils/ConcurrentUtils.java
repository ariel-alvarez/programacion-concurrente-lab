package edu.unq.pconc.utils;

/**
 * Algunos métodos básicos que vamos usando en los ejemplos, meramente para mejorar su legibilidad.
 */
public class ConcurrentUtils {

    public static void print(String texto) {
        System.out.println(texto);
    }
    public static void print(Object texto) {
        System.out.println(texto.toString());
    }

    public static void dormir(int segundos) {
        try {
            Thread.sleep(segundos*1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
